//
// Created by 高志阳 on 2023/3/25.
//

#include "PCB.h"
#include <random>
/*
 *  int ID;
    int timeRequire;
    int timeUsed;
    State state;
*/
int PCB::allocationId = 0;

PCB::PCB() {
    this->ID = allocationId;
    this->state = State::created;
    std::random_device rd;  //如果可用的话，从一个随机数发生器上获得一个真正的随机数
    std::mt19937 gen(rd()); //gen是一个使用rd()作种子初始化的标准梅森旋转算法的随机数发生器
    std::uniform_int_distribution<> distrib(1, 8);
    this->timeRequire = distrib(gen);
    this->timeUsed = 0;

    PCB::allocationId++;
}


void PCB::beScheduling() {
    //开始运行程序
    this->state = State::running;
    this->addTimeUsed();
    this->subTimeReq();

    //程序执行完成
    if(this->timeRequire == 0) {
        this->state = State::completed;
        return;
    }
    this->state = State::ready;
}

void PCB::addTimeUsed() {
    this->timeUsed++;
}

void PCB::subTimeReq() {
    this->timeRequire--;
}

int PCB::getId() const {
    return ID;
}

int PCB::getTimeRequire() const {
    return timeRequire;
}

int PCB::getTimeUsed() const {
    return timeUsed;
}

State PCB::getState() const {
    return state;
}


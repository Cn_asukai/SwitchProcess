//
// Created by 高志阳 on 2023/3/25.
//

#ifndef SWITCHPROCESS_STATE_H
#define SWITCHPROCESS_STATE_H
enum class State {
    created = 0,//new为关键字，所以替换成created
    running,
    ready,
    completed,
    blocked,
    readSuspend,
    blockedSuspend
};
#endif //SWITCHPROCESS_STATE_H

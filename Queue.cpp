//
// Created by 高志阳 on 2023/3/25.
//
#include <iostream>
#include <string>

#include "Queue.h"

PCB Queue::front() {
    return this->PCBQue.front();
}

PCB Queue::pop() {
    PCB front = this->front();
    this->PCBQue.pop_front();
    return front;
}

void Queue::push(PCB pcb) {
    this->PCBQue.emplace_back(pcb);
    return;
}

void Queue::show() {
    std::cout << "当前队列中：" << std::endl;
    for (auto it: this->PCBQue) {
        std::string state;
        switch (it.getState()) {
            case State::running :
                state = "running";
            case State::ready :
                state = "ready";
            case State::completed :
                state = "completed";
            case State::created :
                state = "created";
        }
        std::cout << "ID : " << it.getId() << " is " << state << " timeRequire : " << it.getTimeRequire() << " timeUsed: " << it.getTimeUsed() << std::endl;
    }
}

bool Queue::isEmpty() {
    return this->PCBQue.empty();
}

Queue::Queue(int processSize) {
    for(int i = 0; i < processSize; i++) {
        PCB pcb;
        this->push(pcb);
    }
}

Queue::Queue() {

}

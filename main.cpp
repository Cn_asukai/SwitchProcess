#include <iostream>
#include <random>
#include "PCB.h"
#include "Queue.h"

int main() {
    std::default_random_engine  e;
    e.seed(time(0));
    int processSize = e() % 10 + 1;
    Queue completedQue, readyQue(processSize);

    while (!readyQue.isEmpty()) {
        readyQue.show();
        auto cur = readyQue.pop();
        cur.beScheduling();
        std::cout << "正在运行第" << cur.getId() << "号进程" << std::endl;
        if(cur.getState() == State::completed) {
            completedQue.push(cur);
        } else if(cur.getState() == State::ready){
            readyQue.push(cur);
        }
    }
    std::cout << "所有进程调度完成！完成队列:" << std::endl;
    completedQue.show();
    return 0;
}

//
// Created by 高志阳 on 2023/3/25.
//

#ifndef SWITCHPROCESS_PCB_H
#define SWITCHPROCESS_PCB_H

#include "State.h"

class PCB {
public:
    PCB();

    void beScheduling();

    int getId() const;

    int getTimeRequire() const;

    int getTimeUsed() const;

    State getState() const;


private:
    void addTimeUsed();

    void subTimeReq();

private:
    int ID;
    int timeRequire;
    int timeUsed;
    State state;

    static int allocationId;
};


#endif //SWITCHPROCESS_PCB_H

//
// Created by 高志阳 on 2023/3/25.
//

#ifndef SWITCHPROCESS_QUEUE_H
#define SWITCHPROCESS_QUEUE_H

#include <deque>

#include "PCB.h"

class Queue {
public:
    Queue();
    Queue(int processSize);

    PCB pop();

    void push(PCB pcb);

    void show();

    bool isEmpty();
private:
    PCB front();

private:
    std::deque<PCB> PCBQue;
};


#endif //SWITCHPROCESS_QUEUE_H
